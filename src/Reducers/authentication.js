import userState from './userState';

const authReducer = (state = userState, action) => {
  switch (action.type) {
    case 'LOGIN_SUCCESS':
      return Object.assign({}, state, {
        isLogin: action.response
      });
    case 'LOGIN_FAIL':
      return Object.assign({}, state, {
        isLogin: action.response
      });
    default:
      return state;
  }
};
export default authReducer;
