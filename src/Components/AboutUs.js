
import React from 'react';
import { Row, Col,Tabs, Tab } from 'react-bootstrap';

class AboutUs extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            key: 1,
        };
        this.handleSelect = this.handleSelect.bind(this);
    }
    handleSelect(key) {
        this.setState({ key });
    }
    renderProfile(){
        return(
            <Row>
                <Col><h1>Profile</h1></Col>
                <Col>
                    <Col>Name: Sachin Nikam</Col>
                    <Col>Address: Pune.</Col>
                </Col>
            </Row>
        );
    }
    renderTeam(){
        return(
            <Row>
                <Col><h1>Team</h1></Col>
                <Col>
                    Team goes here.
                </Col>
            </Row>
        );
    }
    renderContactUs(){
        return(
            <Row>
                <Col><h1>Contact Us</h1></Col>
                <Col>
                    <Col>Email: sachinnikam705@gmail</Col>
                    <Col>Mob. **********</Col>
                </Col>
            </Row>
        );
    }
    render() {
        return (
            <Col>
                <Tabs activeKey={this.state.key} onSelect={this.handleSelect} id="controlled-tab-example">
                    <Tab eventKey={1} title="Profile">
                        {this.renderProfile()}
                    </Tab>
                    <Tab eventKey={2} title="Team">
                        {this.renderTeam()}
                    </Tab>
                    <Tab eventKey={3} title="Contact Us">
                        {this.renderContactUs()}
                    </Tab>
                </Tabs>
            </Col>
        );
    }
}

export default AboutUs;
