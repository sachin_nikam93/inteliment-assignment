
import React from 'react';
import { hashHistory } from 'react-router';
import { Col } from 'react-bootstrap';

class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }
    renderAboutUs(){
        hashHistory.push('aboutus');
    }
    render() {
        return (
           <Col>
            <h2><a onClick={this.renderAboutUs.bind(this)}>About Us</a></h2>
           </Col>
        );
    }
}

export default Home;
